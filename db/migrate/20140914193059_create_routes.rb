class CreateRoutes < ActiveRecord::Migration
  def change
    create_table :routes do |t|
      t.decimal :distance, precision: 6, scale: 4
      t.references :map, index: true
      t.string :origin
      t.string :destination

      t.timestamps
    end

    add_index :routes, :origin
    add_index :routes, :destination
  end
end
