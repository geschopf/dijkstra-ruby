# Logistics system:


* Registration map with cities and their distance *

* Search the smaller route between two cities in the same map, and cost. *

## About exercise
* The exercise was developed using Ruby on rails 4 with use of rails-api gem.
* With the use of rails-api, the rails only carries some middleware used.
* To calculate the shortest path, we used the Dijkstra's algorithm.
* The approach to the registration of vertices (cities) and edges (distance) allows replication between two vertices with edges, because there is no specification or limitations not be more than one path between the vertices.
* The sqlite3 was the database used to be standard.
* If you act a future need for change, it can be exchanged for new bank, including a solution like Neo4J.
* The application receives JSON values ​​registration and search.
* Return the case a search is also a JSON. It should be sent consumption value of the vehicle and value of the cost of fuel.
* A package gems are used in the development focused on improving the quality of code.
* The lib for Dijkstra's algorithm, is located in the lib / dijkstra folder. A modified version of the sample by locating the address: [http://goo.gl/CO3RhU](http://goo.gl/CO3RhU).
* When sent an existing map with values ​​of cities and distance, it will be updated the same.
* There is an option to update an existing map using the update. As it was not covered in the document, only a functional implementation without further details.

## Getting the application

Clone the git repository bitbucket
```Sh
git clone git@bitbucket.org: geschopf / walmart-challenge.git
```
Installing dependencies
```Sh
bundle install
```

Creating the Database
```Sh
rake db: create
```

Running database migration data
```Sh
rake db: migrate
```
To run the tests
```Sh
bin / rspec
```

To run the WEBrick server \ o
```Sh
s rails
```
## Using
Running the local server WEBrick the default address is: http: // localhost: 3000

** Remember that the application responds only casdastro values ​​and query in JSON format **

** All orders must have the Content-Type header to the value of "application / JSON." **

### Creating a map

Structure of a POST to create a map with routes or adiconar there is an existing map name sent new routes
```
* * Address: http: // localhost: 3000 / maps
{ "Map":
  {
    "Name": ": name_map"
    "Routes": [
                { "Origin": ": city_origin", "destination": ": city_destination", "distance", ": distance_value"},
                { "Origin": ": city_origin", "destination": ": city_destination", "distance", ": distance_value"},
                { "Origin": ": city_origin", "destination": ": city_destination", "distance", ": distance_value"},
                { "Origin": ": city_origin", "destination": ": city_destination", "distance", ": distance_value"},
                { "Origin": ": city_origin", "destination": ": city_destination", "distance", ": distance_value"},
                { "Origin": ": city_origin", "destination": ": city_destination", "distance", ": distance_value"}
              ]
  }
}
```

* Explanation of the parameters *

*: Map_name = ** Name ** map. Example: SP, NY, RJ, BW .....
*: City_origin = home city name. Examples: São Paulo, Sorocaba, Campinas, Rio de Janeiro, Bitburg-Prüm ....
*: City_destination = destination city name (nearby). Examples: Cotia, Niteroi, Neuwied ....
*: Distance_value = Distance between the cities of origin and destination. To create the route. allowed decimal values.

*** Note **: ** Using a dot (. *) ** For separating the whole part of the decimal part is as standard *.

#### Examples:

##### Using the POST command
```
 POST -H "Content-Type: application / json" http: // localhost: 3000 / maps
{ "Map": { "name": "SP", "routes": [{ "origin", "A", "destination", "B", "distance": "10"}, { "origin": "B", "destination", "D", "distance": "15"}, { "origin", "A", "destination", "C", "distance", "02.10"}, { "origin "," C "," destination "," D "," distance ":" 30 "}, {" origin "," B "," destination "," E "," distance ":" 50 "}, { "origin", "D", "destination", "E", "distance": "30"}]}}
```

##### Using the CURL command
```
curl -v -H "Accept: application / json" H "Content-type: application / json" -X -d POST '{ "map": { "name": "SP", "routes": [{ " origin "," A "," destination "," B "," distance ":" 10 "}, {" origin "," B "," destination "," D "," distance ":" 15 "}, { "origin", "A", "destination", "C", "distance": "20"}, { "origin", "C", "destination", "D", "distance", "30" }, { "origin", "B", "destination", "E", "distance": "150"}, { "origin", "D", "destination", "E", "distance", " 30 "}]}} 'http: // localhost: 3000 / maps
```

##### RESPONSE
a JSON object with the saved map.

```
{ "Id": 1, "name": "SP", "created_at": "2014-09-23T18: 04: 41.019Z", "updated_at": "2014-09-23T18: 04: 41.019Z"}
```

### Querying origin and destination of a map and fuel consumption
Calculate the best route and the cost between two cities of a map.
Also passing vehicle consumption more fuel value.


Structure of a GET to see a map with route originating and destination.

```
* * Address: http: // localhost: 3000 / maps /: map_name / shortest-route \ origin \ =: city_origin \ & destination \ =: city_destination \ & oil_consumption \ =: oil_consumption \ & fuel_price \ =: oil_price
```
* Explanation of the parameters *

*: Map_name = ** Name ** map. Example: SP, NY, RJ, BW .....
*: City_origin = home city name. Examples: São Paulo, Sorocaba, Campinas, Rio de Janeiro, Bitburg-Prüm ....
*: City_destination = destination city name (nearby). Examples: Cotia, Niteroi, Neuwied ....
*: Oil_consumption = vehicle consumption by value oil_price field. Example Km per liter.
   It allows decimal values.
*: Oil_price = Price value of fuel per oil_consumption unit of measure. Example 2.1 per liter.

It allows decimal values.

*** Note **: ** Using a dot (. *) ** For separating the whole part of the decimal part is as standard *.

#### Examples:

##### Using the GET command
```
GET http: // localhost: 3000 / maps / SP / shortest-route \ origin \ = A \ & destination \ = D \ & oil_consumption \ = 10.5 \ & oil_price \ = 4?
```
##### Using the CURL command
```
-i curl -H "Accept: application / json" http: // localhost: 3000 / maps / SP / shortest-route \ origin \ = A \ & destination \ = D \ & oil_consumption \ = 10.5 \ & oil_price \ = 4
```

##### RESPONSE
A JSON content with the route of shortest path between points, based on the distance (Dijkstra's algorithm).
```
{ "Shortest_route" "A, B, D", "cost", "10:00"}
```


*** Note: If Windows operating system, I recommend following these steps: [http://simplesideias.com.br/usando-o-vagrant-como-ambiente-de-desenvolvimento-no-windows](http:// simplesideias.com.br/usando-o-vagrant-como-ambiente-de-desenvolvimento-no-windows)***