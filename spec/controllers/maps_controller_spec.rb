require 'rails_helper'

RSpec.describe MapsController, type: :controller do
  describe 'POST#create' do
    context 'with valid attributes' do
      let(:map_name) { Faker::Address.state_abbr }
      let(:map_attributes) { JSON.parse(FactoryGirl.build(:map, :with_routes, name: map_name).to_json(include: :routes)) }
      let(:map_attributes_update_routes) { JSON.parse(FactoryGirl.build(:map, :with_routes, name: map_name).to_json(include: :routes)) }

      it 'creates a new map' do
        expect { post :create, { map: map_attributes }, format: :json }.to change { Map.count }.by(1)
        expect(response).to be_success

        map_saved = Map.where(name: map_name).first
        expect { post :create, { map: map_attributes_update_routes }, format: :json }.to change { Route.where(map_id: map_saved.id).count }.by(1)
      end
    end

    context 'with invalid attributes' do
      let(:map_name) {  nil }
      let(:map_attributes) { JSON.parse(FactoryGirl.build(:map, :with_routes, name: map_name).to_json(include: :routes)) }

      it 'does not save the map' do
        expect { post :create, { map: map_attributes }, format: :json }.to change { Map.count }.by(0)
        expect(response.status).to eq(422)
      end
    end
  end

  context 'without routes' do
    let(:map_name) {  Faker::Address.state_abbr }
    let(:map_attributes) {  JSON.parse(FactoryGirl.build(:map, name: map_name).to_json(include: :routes)) }

    it 'does not save the map' do
      expect { post :create, { map: map_attributes }, format: :json }.to change { Map.count }.by(0)
      expect(response.status).to eq(422)
    end
  end

  describe 'PATCH#update' do
    context 'with valid attributes' do
      let(:new_map) { FactoryGirl.build(:map, :with_routes, name: 'SP') }
      let(:map) { FactoryGirl.create(:map, :with_routes, name: 'SP') }
      let(:map_attributes) { JSON.parse(new_map.to_json(include: :routes)) }

      before :each do
        patch :update, id: map.id, map: map_attributes
      end

      it 'update the contact' do
        updated_map = Map.find(map.id)
        expect(updated_map.name).to eq(new_map.name)
      end

      it 'respond with 200' do
        expect(response).to be_success
      end
    end

    context 'with invalid attributes' do
      let(:new_attributes) {  FactoryGirl.attributes_for(:map, name: nil) }
      let(:map) { FactoryGirl.create(:map, :with_routes, name: 'SP') }

      before :each do
        patch :update, id: map, map: new_attributes
      end
      it 'does not update the map' do
        updated_map = Map.find(map.id)
        expect(updated_map.name).to_not eq new_attributes[:name]
      end

      it 'respond with 422' do
        expect(response.status).to eq(422)
      end
    end
  end

  describe 'GET#shortest_route' do
    context 'Looking shortest path map' do
      let(:map_name) { Faker::Address.state_abbr }

      before do
        FactoryGirl.create(:map, name: map_name, routes: [
          FactoryGirl.create(:route, origin: 'A', destination: 'B', distance: 10),
          FactoryGirl.create(:route, origin: 'B', destination: 'D', distance: 15),
          FactoryGirl.create(:route, origin: 'A', destination: 'C', distance: 20),
          FactoryGirl.create(:route, origin: 'C', destination: 'D', distance: 30),
          FactoryGirl.create(:route, origin: 'B', destination: 'E', distance: 50),
          FactoryGirl.create(:route, origin: 'D', destination: 'E', distance: 30)
        ])
      end

      it 'will show the shortest path' do
        get :shortest_route, map: map_name, origin: 'A', destination: 'D',
        oil_consumption: 10, oil_price: 2.5
        expect(JSON.parse(response.body)['shortest_route']).to eq(%w(A B D).join(','))
        expect(response).to be_success
      end

      it 'will show the cost of the trip' do
        get :shortest_route, map: map_name, origin: 'A', destination: 'D',
        oil_consumption: 10, oil_price: 2.5
        expect(JSON.parse(response.body)['cost']).to eq(6.25)
      end

      it 'will return 422 when pass a empty map' do
        get :shortest_route, map: '', origin: 'A', destination: 'D', oil_consumption: 10, oil_price: 2.5
        expect(response.status).to eq(422)
      end
    end
  end
end
