FactoryGirl.define do
  factory :route do
    distance { Faker::Number.number(2) }
    origin { Faker::Address.city }
    destination { Faker::Address.city }
  end
end
