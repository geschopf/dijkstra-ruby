FactoryGirl.define do
  factory :map do
    trait :name do
      name 'SP'
    end
    trait :with_routes do
      after(:create, :build) do |map|
        map.routes << FactoryGirl.build_list(:route, 1, map: map)
      end
    end
  end
end
