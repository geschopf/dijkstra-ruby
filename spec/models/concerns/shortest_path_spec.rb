require 'spec_helper'

describe ShortestPath do
  class MapModel
    include ShortestPath
  end

  let(:map) do
    FactoryGirl.create(:map, name: 'SP', routes: [
      FactoryGirl.create(:route, origin: 'A', destination: 'B', distance: 10),
      FactoryGirl.create(:route, origin: 'B', destination: 'D', distance: 15),
      FactoryGirl.create(:route, origin: 'A', destination: 'C', distance: 20),
      FactoryGirl.create(:route, origin: 'C', destination: 'D', distance: 30),
      FactoryGirl.create(:route, origin: 'B', destination: 'E', distance: 50),
      FactoryGirl.create(:route, origin: 'D', destination: 'E', distance: 30)
    ])
  end

  describe '#shortest_path' do
    it 'returns the shortest route' do
      expected_route = %w(A B D)
      shortest_routes = map.shortest_path('A', 'D')
      expect(shortest_routes).to match_array(expected_route)
    end
  end

  describe '#route_cost' do
    it 'returns the cost of the route' do
      expect(map.path_cost('A', 'D', 10, 2.5)).to eq 6.25
    end
  end

end
