require 'rails_helper'

RSpec.describe Map, type: :model do
  before do
    @map = FactoryGirl.create(:map, name: 'SP', routes: [
      FactoryGirl.create(:route, origin: 'A', destination: 'B', distance: 10),
      FactoryGirl.create(:route, origin: 'B', destination: 'D', distance: 15),
      FactoryGirl.create(:route, origin: 'A', destination: 'C', distance: 20),
      FactoryGirl.create(:route, origin: 'C', destination: 'D', distance: 30),
      FactoryGirl.create(:route, origin: 'B', destination: 'E', distance: 50),
      FactoryGirl.create(:route, origin: 'D', destination: 'E', distance: 30)
    ])
  end

  context 'Seeks the shortest route' do
    describe '#shortest_path' do
      it 'returns the shortest path' do
        expected_route = %w(A B D)
        shortest_routes = @map.shortest_path('A', 'D')
        expect(shortest_routes).to match_array(expected_route)
      end
    end

    describe '#path_cost' do
      it 'returns the cost of the path' do
        expect(@map.path_cost('A', 'D', 10, 2.5)).to eq 6.25
      end
    end
  end

  context 'Validations' do
    it 'should validate presence of :name' do
      expect(subject).to validate_presence_of :name
    end
    it 'should validate uniqueness of :name' do
      expect(subject).to validate_uniqueness_of :name
    end
  end
end
