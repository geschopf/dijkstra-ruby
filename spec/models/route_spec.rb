require 'rails_helper'

RSpec.describe Route, type: :model do
  it { should belong_to(:map) }
  it { should validate_presence_of(:distance) }
  it { should validate_numericality_of(:distance).is_greater_than(0) }
  it { should validate_presence_of(:origin) }
  it { should validate_presence_of(:destination) }
end
