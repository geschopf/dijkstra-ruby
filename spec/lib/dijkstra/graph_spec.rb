require 'rails_helper'

describe Dijkstra::Graph do
  let(:graph) { Dijkstra::Graph.new }
  it 'create a empty edges list when initialize' do
    expect(graph.edges).to eq []
  end

  describe '#connect' do
    it 'create a edge with source, destination and length and add to edges list' do
      graph.connect('A', 'B', 10)
      expect(graph.edges.size).to be 1
      expect(graph.edges.first.origin).to eq 'A'
      expect(graph.edges.first.destination).to eq 'B'
      expect(graph.edges.first.length).to be 10
    end

    it 'create a edge with source, destination and length and add to edges list' do
      graph.connect('A', 'B', 10)
      graph.connect('B', 'C', 10)
      graph.connect('B', 'D', 10)
      graph.connect('C', 'D', 5)

      expect(graph.edges.size).to be 4
      expect(graph.edges.first.origin).to eq 'A'
      expect(graph.edges.first.destination).to eq 'B'
      expect(graph.edges.first.length).to be 10
    end

    it 'create a edge with source, destination and length and add to edges list' do
      graph.connect('A', 'B', 10)
      graph.connect('A', 'B', 1)
      graph.connect('B', 'C', 10)
      graph.connect('B', 'D', 10)
      graph.connect('C', 'D', 5)

      expect(graph.edges.size).to be 5
      expect(graph.edges.first.origin).to eq 'A'
      expect(graph.edges.last.destination).to eq 'D'
      expect(graph.edges.first.length).to be 10
      expect(graph.edges.second.length).to be 1

    end

    it 'add the node to the graph' do
      expect(graph.include?('A')).to be_falsey
      expect(graph.include?('B')).to be_falsey
      graph.connect('A', 'B', 10)
      expect(graph.include?('A')).to be_truthy
      expect(graph.include?('B')).to be_truthy
    end
  end

  describe '#connect_mutually' do
    it 'have 2 itens in edges lists' do
      graph.connect_mutually('A', 'B', 10)
      expect(graph.edges.size).to be 2
    end
    it 'have correct itens in edges list' do
      graph.connect_mutually('A', 'B', 10)
      expect(graph.edges.first.origin).to eq 'A'
      expect(graph.edges.first.destination).to eq 'B'
      expect(graph.edges.first.length).to be 10

      expect(graph.edges.second.origin).to eq 'B'
      expect(graph.edges.second.destination).to eq 'A'
      expect(graph.edges.second.length).to be 10
    end
  end

  describe '#neighbors' do
    it 'list neighbors' do
      graph.connect_mutually('A', 'B', 10)
      graph.connect_mutually('A', 'C', 5)
      graph.connect_mutually('B', 'D', 1)
      graph.connect_mutually('D', 'A', 2)
      graph.connect_mutually('E', 'F', 2)

      expected_neighbors = %w(B C D)

      expect(graph.neighbors('A')).to include(*expected_neighbors)
    end
  end

  describe '#length_between' do
    it 'calculates the length between two points' do
      graph.connect_mutually('A', 'B', 20)
      graph.connect_mutually('A', 'D', 10)
      expect(graph.length_between('A', 'D')).to be 10
    end
  end

  describe '#dijkstra' do
    it 'get the shortest path' do
      graph.connect_mutually('A', 'B', 7)
      graph.connect_mutually('A', 'C', 9)
      graph.connect_mutually('A', 'D', 14)
      graph.connect_mutually('B', 'C', 10)
      graph.connect_mutually('B', 'D', 2)

      expected_path = %w(A B D)
      shortest_path = graph.dijkstra('A', 'D')
      expect(shortest_path[:path]).to include(*expected_path)
      expect(shortest_path[:distance]).to be 9
    end

    it 'get the shortest path' do
      graph.connect_mutually('A', 'B', 7)
      graph.connect_mutually('A', 'C', 9)
      graph.connect_mutually('A', 'D', 14)
      graph.connect_mutually('B', 'C', 10)
      graph.connect_mutually('B', 'D', 2)
      graph.connect_mutually('D', 'F', 2)

      expected_path = %w(A B D F)
      shortest_path = graph.dijkstra('A', 'F')
      expect(shortest_path[:path]).to include(*expected_path)
      expect(shortest_path[:distance]).to be 11
    end
  end
end
