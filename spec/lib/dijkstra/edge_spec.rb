require 'rails_helper'

describe Dijkstra::Edge do
  it 'raise an execption without params' do
    expect { Dijkstra::Edge.new } .to raise_error
  end

  it 'assigns correct variables' do
    edge = Dijkstra::Edge.new('A', 'E', 10)

    expect(edge.origin).to eq 'A'
    expect(edge.destination).to eq 'E'
    expect(edge.length).to be(10)
  end
end
