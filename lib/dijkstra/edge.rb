module Dijkstra
  class Edge
    attr_accessor :origin, :destination, :length

    def initialize(origin, destination, length)
      @origin = origin
      @destination = destination
      @length = length
    end
  end
end
