module Dijkstra
  class Graph < Array
    attr_reader :edges

    def initialize
      @edges = []
    end

    def connect(origin, destination, length)
      push origin unless include?(origin)
      push destination unless include?(destination)
      @edges.push Edge.new(origin, destination, length)
    end

    def connect_mutually(vertex1, vertex2, length)
      connect vertex1, vertex2, length
      connect vertex2, vertex1, length
    end

    def neighbors(vertex)
      @edges.map { |edge| edge.destination if edge.origin == vertex } .reject(&:nil?).uniq
    end

    def length_between(origin, destination)
      @edges.map do |edge|
        return edge.length if edge.origin == origin && edge.destination == destination
      end
      nil
    end

    def dijkstra(origin, destination)
      distances = {}
      previouses = {}
      map do |vertex|
        distances[vertex] = nil
        previouses[vertex] = nil
      end
      distances[origin] = 0
      vertices = clone
      until vertices.empty?
        nearest_vertex = vertices_reduce(vertices, distances)

        break unless distances[nearest_vertex]
        if nearest_vertex == destination
          path = get_path(previouses, origin, destination)
          return { path: path, distance: distances[destination] }
        end

        neighbors = vertices.neighbors(nearest_vertex)

        neighbors_distances(neighbors, distances, nearest_vertex, previouses, vertices)

        vertices.delete nearest_vertex
      end
    end

    private

    def vertices_reduce(vertices, distances)
      vertices.reduce do |a, b|
        next b unless distances[a]
        next a unless distances[b]
        next a if distances[a] < distances[b]
        b
      end
    end

    def neighbors_distances(neighbors, distances, nearest_vertex, previouses, vertices)
      neighbors.map do |vertex|
        alt = distances[nearest_vertex] + vertices.length_between(nearest_vertex, vertex)
        if distances[vertex].nil? || alt < distances[vertex]
          distances[vertex] = alt
          previouses[vertex] = nearest_vertex
        end
      end
    end

    def get_path(previouses, origin, destination)
      path = get_path_recursively(previouses, origin, destination)
      path.is_a?(Array) ? path.reverse : path
    end

    def get_path_recursively(previouses, origin, destination)
      return origin if origin == destination
      fail ArgumentError, "No path from #{origin} to #{destination}" if previouses[destination].nil?
      [destination, get_path_recursively(previouses, origin, previouses[destination])].flatten
    end
  end
end
