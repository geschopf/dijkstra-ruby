module ShortestPath
  extend ActiveSupport::Concern

  def shortest_path(origin, destination)
    stored_shortest_path(origin, destination)[:path]
  end

  def path_cost(origin, destination, oil_consumption, oil_price)
    (stored_shortest_path(origin, destination)[:distance].to_f / oil_consumption.to_f) * oil_price.to_f
  end

  private

  def current_graph
    graph ||= begin
      graph = Dijkstra::Graph.new
      routes.map { |route| graph.connect_mutually(route.origin, route.destination, route.distance) }
      graph
    end
  end

  def stored_shortest_path(origin, destination)
    stored_shortest_path ||= {}
    stored_shortest_path["#{origin}_#{destination}"] ||= current_graph.dijkstra(origin, destination)
  end
end
