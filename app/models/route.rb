class Route < ActiveRecord::Base
  validates :origin, presence: true
  validates :destination, presence: true
  validates :distance, presence: true, numericality: { greater_than: 0 }

  belongs_to :map
end
