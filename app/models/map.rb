class Map < ActiveRecord::Base
  include ShortestPath
  has_many :routes
  accepts_nested_attributes_for :routes

  validates :name, presence: true, uniqueness: true
  validates :routes, presence: true
end
