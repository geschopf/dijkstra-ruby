class MapsController < ApplicationController
  before_action :assign_routes, only: [:update, :create]
  before_action :assign_or_create, only: [:update, :create]
  before_action :map_update_attributes, only: [:update, :create]

  def create
    if @map.save
      render json: @map, status: 200
    else
      render json: @map.errors, status: 422
    end
  end

  def update
    if @map.update_attributes(map_params)
      render json: @map, status: 200
    else
      render json: @map.errors, status: 422
    end
  end

  def shortest_route
    map = Map.where(name: params[:map]).first
    if map
      shortest_route = {
        shortest_route: map.shortest_path(params[:origin], params[:destination]).join(','),
        cost: map.path_cost(params[:origin], params[:destination], params[:oil_consumption], params[:oil_price])
      }
      render json: shortest_route
    else
      render json: { error: 'Not Found' },  status: 422
    end
  end

  private

  def assign_or_create
    @map = Map.find(params[:id]) if params.key?(:id)
    @map = Map.find_or_create_by(name: params[:map][:name]) if @map.nil?

    render json: { error: 'Not Found' },  status: 422 if @map.nil?
  end

  def assign_routes
    params[:map][:routes_attributes] ||= params[:map].delete(:routes)
  end

  def map_update_attributes
    @map.update_attributes(map_params)
  end

  def map_params
    params.require(:map).permit(:name, routes_attributes: [:origin, :destination, :distance])
  end
end
