Rails.application.routes.draw do
  resources :maps, only: [:create, :update], constraints: { format: 'json' }
  get '/maps/:map/shortest-route', to: 'maps#shortest_route', as: 'shortest_route', constraints: { format: 'json' }
end
